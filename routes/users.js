var express = require('express');
var router = express.Router();
var mongo=require('./MongoConnection');
var config=require("../config/labsConfig");
var async = require('async');

var db=null;
mongo.createConnection(config.url, function (err,db1) {
  if(err)
  {
    throw err;
  }else
  {
    //console.log("connected to mongo..");
    db = db1;
  }

});

/* GET users listing. */
//router.get('/', function(req, res) {
//  res.render('userDetails')
//});
router.get('/userDetails',function(req,res){
  //console.log(req);
  var fdd=  req.query.fdate;
    var tdd=req.query.tdate;
  db.collection('user_master').find({'date.dateStamp':{'$gte':parseInt(fdd),'$lte':parseInt(tdd)}}).sort({'date.dateStamp':-1}).toArray(function(err,data){
        if (err) {
          throw err;
        }
        else {
          res.send(data);
        }
      });
});

module.exports = router;
