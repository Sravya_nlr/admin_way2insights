var express = require('express');
var router = express.Router();
var AES = require("./aes.js");
var mongo=require('./MongoConnection');
var config=require("../config/labsConfig");
// var config=require("../config/liveConfig"); //uncomment for liveconfig

var db=null;
mongo.createConnection(config.url, function (err,db1) {
    if(err)
    {
      throw err;
    }else
    {
      console.log("connected to mongo..");
      db = db1;
    }
        
});

router.get('/', function(req, res) {
  // res.render('index', { title: 'Express' });
  var cookie=req.cookies.encEmail;
  if(cookie){
    res.render('domains.html')
  }
  else{
    res.render('login2')
  }
});
router.get('/cookie',function(req,res){
  var cookie=req.cookies.encEmail;
  if(cookie === undefined){
      console.log("sdffff")
      res.render('login2')
  }
  else{
      var dec_con = AES.decrypt(cookie);
      //console.log(dec_con);
      dec_con=JSON.parse(dec_con);
      //console.log(dec_con.email);
      var checkMail=dec_con.email;
      if(checkMail === "sravya.n@way2online.co.in" || checkMail === "murali@way2online.net" || checkMail === "surya.kumar@way2online.co.in" || checkMail === "sudheer.k@way2online.co.in" || checkMail ==="rithwik.k@way2online.co.in" || checkMail === "veerakrishna.p@way2online.co.in") {
          res.send(dec_con);
      }
      else{
          res.send({"user":"not our user"});
      }
  }
});

router.get('/logout', function(req, res) {
  // res.render('index', { title: 'Express' });

  // console.log(dec_con);
  res.clearCookie('encEmail');
  res.render('login2')

});
router.post('/email',function(req,res){
  console.log(req.body);
    var checkMail=req.body.email;
    if(checkMail === "sravya.n@way2online.co.in" || checkMail === "murali@way2online.net" || checkMail === "surya.kumar@way2online.co.in" || checkMail === "sudheer.k@way2online.co.in" || checkMail ==="rithwik.k@way2online.co.in" || checkMail === "veerakrishna.p@way2online.co.in"){
        var email = {email:req.body.email,imageUrl:req.body.imageUrl,profileName:req.body.profileName};
        var encrypt_email = AES.encrypt(JSON.stringify(email));
        res.cookie("encEmail",encrypt_email);
        res.send("cookie is set successfully");
    }
    else{
        res.send({'user':"user not exists"});
    }
  // res.render('chart-js')
});

router.post('/dailydomains', function(req, res) {
    var Count = req.body.count;
    var Sort = req.body.sort;
    db.collection("dom_dayStats").find({Date:req.body.date ,processed:{$gt:1}}, {})
        .sort({count:Sort}).skip(Count*10).limit(10).toArray(function (err, data) {
            if(err)
            {
                throw err;
            }
            else
            {
                Resultprocessing(data,function(err,response){
                    if(err){
                        throw err;
                    }else{
                        res.send({a:JSON.stringify(response)});
                    }

                })
            }

    });

})

router.post('/dailydomains1', function(req, res) {
    var Count = req.body.count;
    var Sort = req.body.sort;
    db.collection("dom_dayStats").find({Date:req.body.date ,processed:{$gt:1}}, {})
        .sort({processed:Sort}).skip(Count*10).limit(10).toArray(function (err, data) {
        if(err)
        {
            throw err;
        }
        else
        {
            Resultprocessing(data,function(err,response){
                if(err){
                    throw err;
                }else{
                    res.send({a:JSON.stringify(response)});
                }

            })
        }

    });

})

router.post('/DomainSearch', function(req, res) {
    var dom = req.body.domain;
    var dat = req.body.date;
    db.collection("domain_master").findOne({D_name: dom}, function(err, result) {
        if (err) throw err;
        else {
            if (result !== null && result !== undefined) {
                db.collection("dom_dayStats").find({D_id: result._id, Date: dat, processed:{$gte: 1}}).toArray(function (err, result1) {
                    if (err) throw err;
                    else {
                        Resultprocessing(result1, function (err, stats) {
                            if (err) throw err;
                            else {
                                console.log(JSON.stringify(stats));
                                res.send({b: JSON.stringify(stats)});
                            }
                        });
                    }

                });
            }
            else {
                res.send({b:"oops"});
            }
        }


    });

});

function Resultprocessing(result,cb)
{
    var i = 0;
    var gmail_count = 0;
    var yahoo_count = 0;
    var total_inbox = 0;
    var total_spam = 0;
    var total_volume = 0;
    var processed = 0;
    var opens = 0;
    var unread_trash = 0;
    var unread_total = 0;
    var Topens = 0;
    var final_domains = [];
    if (result.length != 0) {
        looping(i);
    }
    else {
        cb(null,[]);
    }

    function looping(i)
    {
        if(i <= (result.length)-1)
        {
            var gpromotion,ypromotion;
            var gstarred,ystarred;
            var gsocial,ysocial;
            var gupdates,yupdates;
            var ginbox,yinbox;
            var gtrash,ytrash;
            var gimportant,yimportant;
            var gspam,yspam;
            var gother,yother;
            var gr_promotion,yr_promotion;
            var gr_starred,yr_starred;
            var gr_social,yr_social;
            var gr_updates,yr_updates;
            var gr_inbox,yr_inbox;
            var gr_trash,yr_trash;
            var gr_important,yr_important;
            var gr_spam,yr_spam;
            var gr_other,yr_other;
            var gur_promotion,yur_promotion;
            var gur_starred,yur_starred;
            var gur_social,yur_social;
            var gur_updates,yur_updates;
            var gur_inbox,yur_inbox;
            var gur_trash,yur_trash;
            var gur_important,yur_important;
            var gur_spam,yur_spam;
            var gur_other,yur_other;


            if(result[i].hasOwnProperty('processed'))
            {
                if(result[i].hasOwnProperty('gmail'))
                {
                    if(result[i].gmail.hasOwnProperty('promotions'))
                    {
                        gpromotion = result[i].gmail.promotions;
                    }
                    else
                    {
                        gpromotion = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('starred'))
                    {
                        gstarred = result[i].gmail.starred;
                    }
                    else
                    {
                        gstarred = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('social'))
                    {
                        gsocial = result[i].gmail.social;
                    }
                    else
                    {
                        gsocial = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('updates'))
                    {
                        gupdates = result[i].gmail.updates;
                    }
                    else
                    {
                        gupdates = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('inbox'))
                    {
                        ginbox = result[i].gmail.inbox;
                    }
                    else
                    {
                        ginbox = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('trash'))
                    {
                        gtrash = result[i].gmail.trash;
                    }
                    else
                    {
                        gtrash = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('important'))
                    {
                        gimportant = result[i].gmail.important;
                    }
                    else
                    {
                        gimportant = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('spam'))
                    {
                        gspam = result[i].gmail.spam;
                    }
                    else
                    {
                        gspam = 0;
                    }
                    if(result[i].gmail.hasOwnProperty('other'))
                    {
                        gother = result[i].gmail.other;
                    }
                    else
                    {
                        gother = 0;
                    }
                    /*console.log(gspam+" "+gimportant+" "+gupdates+" "+gstarred+" "+gsocial+" "+gpromotion+" "+gtrash+" "+gother+" "+ginbox)*/
                }
                else
                {
                    ginbox=gspam=gimportant=gupdates=gstarred=gsocial=gpromotion=gtrash=gother=0;
                }
                if(result[i].hasOwnProperty('yahoo'))
                {
                    if(result[i].yahoo.hasOwnProperty('promotions'))
                    {
                        ypromotion = result[i].yahoo.promotions;

                    }
                    else
                    {
                        ypromotion = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('starred'))
                    {
                        ystarred = result[i].yahoo.starred;
                    }
                    else
                    {
                        ystarred = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('social'))
                    {
                        ysocial = result[i].yahoo.gsocial;
                    }
                    else
                    {
                        ysocial = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('updates'))
                    {
                        yupdates = result[i].yahoo.updates;
                    }
                    else
                    {
                        yupdates = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('inbox'))
                    {
                        yinbox = result[i].yahoo.inbox;
                    }
                    else
                    {
                        yinbox = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('trash'))
                    {
                        ytrash = result[i].yahoo.trash;
                    }
                    else
                    {
                        ytrash = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('important'))
                    {
                        yimportant = result[i].yahoo.important;
                    }
                    else
                    {
                        yimportant = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('spam'))
                    {
                        yspam = result[i].yahoo.spam;
                    }
                    else
                    {
                        yspam = 0;
                    }
                    if(result[i].yahoo.hasOwnProperty('other'))
                    {
                        yother = result[i].yahoo.other;
                    }
                    else
                    {
                        yother = 0;
                    }
                }
                else
                {
                    yinbox=yspam=yimportant=yupdates=ystarred=ysocial=ypromotion=ytrash=yother=0;
                }
                if(result[i].hasOwnProperty('gmail_read'))
                {
                    if(result[i].gmail_read.hasOwnProperty('promotions'))
                    {
                        gr_promotion = result[i].gmail_read.promotions;
                    }
                    else
                    {
                        gr_promotion = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('starred'))
                    {
                        gr_starred = result[i].gmail_read.starred;
                    }
                    else
                    {
                        gr_starred = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('social'))
                    {
                        gr_social = result[i].gmail_read.social;
                    }
                    else
                    {
                        gr_social = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('updates'))
                    {
                        gr_updates = result[i].gmail_read.updates;
                    }
                    else
                    {
                        gr_updates = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('inbox'))
                    {
                        gr_inbox = result[i].gmail_read.inbox;
                    }
                    else
                    {
                        gr_inbox = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('trash'))
                    {
                        gr_trash = result[i].gmail_read.trash;
                    }
                    else
                    {
                        gr_trash = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('important'))
                    {
                        gr_important = result[i].gmail_read.important;
                    }
                    else
                    {
                        gr_important = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('spam'))
                    {
                        gr_spam = result[i].gmail_read.spam;
                    }
                    else
                    {
                        gr_spam = 0;
                    }
                    if(result[i].gmail_read.hasOwnProperty('other'))
                    {
                        gr_other = result[i].gmail_read.other;
                    }
                    else
                    {
                        gr_other = 0;
                    }
                    /*console.log(gspam+" "+gimportant+" "+gupdates+" "+gstarred+" "+gsocial+" "+gpromotion+" "+gtrash+" "+gother+" "+ginbox)*/
                }
                else
                {
                    gr_inbox=gr_spam=gr_important=gr_updates=gr_starred=gr_social=gr_promotion=gr_trash=gr_other=0;
                }
                if(result[i].hasOwnProperty('yahoo_read'))
                {
                    if(result[i].yahoo_read.hasOwnProperty('promotions'))
                    {
                        yr_promotion = result[i].yahoo_read.promotions;

                    }
                    else
                    {
                        yr_promotion = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('starred'))
                    {
                        yr_starred = result[i].yahoo_read.starred;
                    }
                    else
                    {
                        yr_starred = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('social'))
                    {
                        yr_social = result[i].yahoo_read.gsocial;
                    }
                    else
                    {
                        yr_social = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('updates'))
                    {
                        yr_updates = result[i].yahoo_read.updates;
                    }
                    else
                    {
                        yr_updates = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('inbox'))
                    {
                        yr_inbox = result[i].yahoo_read.inbox;
                    }
                    else
                    {
                        yr_inbox = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('trash'))
                    {
                        yr_trash = result[i].yahoo_read.trash;
                    }
                    else
                    {
                        yr_trash = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('important'))
                    {
                        yr_important = result[i].yahoo_read.important;
                    }
                    else
                    {
                        yr_important = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('spam'))
                    {
                        yr_spam = result[i].yahoo_read.spam;
                    }
                    else
                    {
                        yr_spam = 0;
                    }
                    if(result[i].yahoo_read.hasOwnProperty('other'))
                    {
                        yr_other = result[i].yahoo_read.other;
                    }
                    else
                    {
                        yr_other = 0;
                    }
                }
                else
                {
                    yr_inbox=yr_spam=yr_important=yr_updates=yr_starred=yr_social=yr_promotion=yr_trash=yr_other=0;
                }
                if(result[i].hasOwnProperty('gmail_unread'))
                {
                    if(result[i].gmail_unread.hasOwnProperty('promotions'))
                    {
                        gur_promotion = result[i].gmail_unread.promotions;
                    }
                    else
                    {
                        gur_promotion = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('starred'))
                    {
                        gur_starred = result[i].gmail_unread.starred;
                    }
                    else
                    {
                        gur_starred = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('social'))
                    {
                        gur_social = result[i].gmail_unread.social;
                    }
                    else
                    {
                        gur_social = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('updates'))
                    {
                        gur_updates = result[i].gmail_unread.updates;
                    }
                    else
                    {
                        gur_updates = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('inbox'))
                    {
                        gur_inbox = result[i].gmail_unread.inbox;
                    }
                    else
                    {
                        gur_inbox = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('trash'))
                    {
                        gur_trash = result[i].gmail_unread.trash;
                    }
                    else
                    {
                        gur_trash = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('important'))
                    {
                        gur_important = result[i].gmail_unread.important;
                    }
                    else
                    {
                        gur_important = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('spam'))
                    {
                        gur_spam = result[i].gmail_unread.spam;
                    }
                    else
                    {
                        gur_spam = 0;
                    }
                    if(result[i].gmail_unread.hasOwnProperty('other'))
                    {
                        gur_other = result[i].gmail_unread.other;
                    }
                    else
                    {
                        gur_other = 0;
                    }
                    /*console.log(gspam+" "+gimportant+" "+gupdates+" "+gstarred+" "+gsocial+" "+gpromotion+" "+gtrash+" "+gother+" "+ginbox)*/
                }
                else
                {
                    gur_inbox=gur_spam=gur_important=gur_updates=gur_starred=gur_social=gur_promotion=gur_trash=gur_other=0;
                }
                if(result[i].hasOwnProperty('yahoo_unread'))
                {
                    if(result[i].yahoo_unread.hasOwnProperty('promotions'))
                    {
                        yur_promotion = result[i].yahoo_unread.promotions;

                    }
                    else
                    {
                        yur_promotion = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('starred'))
                    {
                        yur_starred = result[i].yahoo_unread.starred;
                    }
                    else
                    {
                        yur_starred = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('social'))
                    {
                        yur_social = result[i].yahoo_unread.gsocial;
                    }
                    else
                    {
                        yur_social = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('updates'))
                    {
                        yur_updates = result[i].yahoo_unread.updates;
                    }
                    else
                    {
                        yur_updates = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('inbox'))
                    {
                        yur_inbox = result[i].yahoo_unread.inbox;
                    }
                    else
                    {
                        yur_inbox = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('trash'))
                    {
                        yur_trash = result[i].yahoo_unread.trash;
                    }
                    else
                    {
                        yur_trash = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('important'))
                    {
                        yur_important = result[i].yahoo_unread.important;
                    }
                    else
                    {
                        yur_important = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('spam'))
                    {
                        yur_spam = result[i].yahoo_unread.spam;
                    }
                    else
                    {
                        yur_spam = 0;
                    }
                    if(result[i].yahoo_unread.hasOwnProperty('other'))
                    {
                        yur_other = result[i].yahoo_unread.other;
                    }
                    else
                    {
                        yur_other = 0;
                    }
                }
                else
                {
                    yur_inbox=yur_spam=yur_important=yur_updates=yur_starred=yur_social=yur_promotion=yur_trash=yur_other=0;
                }
                processed = result[i].processed;
            }
            else
            {
                processed = 0;
                ginbox=gspam=gimportant=gupdates=gstarred=gsocial=gpromotion=gtrash=gother=0;
                yinbox=yspam=yimportant=yupdates=ystarred=ysocial=ypromotion=ytrash=yother=0;
                gr_inbox=gr_spam=gr_important=gr_updates=gr_starred=gr_social=gr_promotion=gr_trash=gr_other=0;
                yr_inbox=yr_spam=yr_important=yr_updates=yr_starred=yr_social=yr_promotion=yr_trash=yr_other=0;
                gur_inbox=gur_spam=gur_important=gur_updates=gur_starred=gur_social=gur_promotion=gur_trash=gur_other=0;
                yur_inbox=yur_spam=yur_important=yur_updates=yur_starred=yur_social=yur_promotion=yur_trash=yur_other=0;

            }
/*
            console.log(yspam+" "+yimportant+" "+yupdates+" "+ystarred+" "+ysocial+" "+ypromotion+" "+ytrash+" "+yother+" "+yinbox);
*/
            gmail_count = (gupdates+gtrash+gimportant+ginbox+gspam+gsocial+gstarred+gpromotion+gother);
            total_inbox = (gupdates+gtrash+gimportant+ginbox+gsocial+gstarred+gpromotion+gother+yupdates+ytrash+yimportant+yinbox+ysocial+ystarred+ypromotion+yother);
            yahoo_count = (yupdates+ytrash+yimportant+yinbox+yspam+ysocial+ystarred+ypromotion+yother);
            total_spam = (yspam+gspam);
            total_volume = result[i].count;
            opens = (gr_inbox+gr_important+gr_updates+gr_starred+gr_social+gr_promotion+gr_trash+gr_other+yr_inbox+yr_important+yr_updates+yr_starred+yr_social+yr_promotion+yr_trash+yr_other);
            Topens = (gr_spam+yr_spam+gr_inbox+gr_important+gr_updates+gr_starred+gr_social+gr_promotion+gr_trash+gr_other+yr_inbox+yr_important+yr_updates+yr_starred+yr_social+yr_promotion+yr_trash+yr_other)
            unread_trash = (gur_trash+yur_trash);
            unread_total = (gur_inbox+gur_important+gur_updates+gur_starred+gur_social+gur_promotion+gur_trash+gur_other+yur_inbox+yur_important+yur_updates+yur_starred+yur_social+yur_promotion+yur_trash+yur_other);
/*
            console.log(gmail_count+" "+total_inbox+" "+yahoo_count+" "+total_spam+" "+processed+" "+total_volume);
*/
            var tinbox = ((total_inbox/processed)*100).toFixed(2);
            var tspam =  ((total_spam/processed)*100).toFixed(2);
            var openss =  ((opens/total_inbox)*100).toFixed(2);
            var ut = ((unread_trash/unread_total)*100).toFixed(2);
            if (isNaN(openss)) {
               openss = 0;
            }
            if (isNaN(ut)) {
                ut = 0;
            }

            console.log("tinbox "+tinbox+"tspam "+tspam+"openss "+openss+"ut "+ut);

            db.collection("domain_master").findOne({_id: result[i].D_id}, function (err, response) {
            if (err) {
                throw err;
            } else {
                /*
                 console.log(response.D_name+" "+gmail_count+" "+total_inbox+" "+yahoo_count+" "+total_spam+" "+processed+" "+total_volume);
                 */

                final_domains.push({domain: response.D_name,gcount: gmail_count,ycount: yahoo_count,Tinbox: tinbox,Tspam: tspam,Tvolume: total_volume,processed: processed,opens:openss,Tunread_trash:ut});
                i++;
                looping(i);
            }
        });
            //
        }
        else
        {
            cb(null,final_domains);
        }
    }

}

module.exports = router;
