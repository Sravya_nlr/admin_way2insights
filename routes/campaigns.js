var mongo=require('./MongoConnection');
var config=require("../config/labsConfig");
var async = require('async');

var db=null;
mongo.createConnection(config.url, function (err,db1) {
    if(err)
    {
        throw err;
    }else
    {
        console.log("connected to mongo..");
        db = db1;
    }

});
module.exports=function(app){


    app.get('/campdata',function(req,res){
        var tdata=[];
        //console.log(JSON.stringify(req.query)+"   query");
        async.waterfall([
            function(callback){
                db.collection('sender_master').findOne({'sender':req.query.sender},function(err,response){
                    if(err){
                        callback(err);
                    }else{
                        if(!response){
                            callback(null,0);
                        }else{
                            callback(null,response._id);
                        }
                    }

                });
            },
            function(did,callback){
                if(did!=0) {
                    db.collection('camp_master').find({'sender':did}).sort({Date: -1}).toArray(function (err, data) {
                        if (err) {
                            callback(err);
                        } else {
                            //console.log('wwwwwwwwwwwww');
                            //console.log(data);
                            var a = [];
                            for (var i = 0; i < data.length; i++) {
                                //console.log(data[i]._id)
                                a.push(data[i]._id);
                            }
                            //console.log('a value is:'+JSON.stringify(a));
                            callback(null, a);
                        }
                    });
                }else{
                    callback(null,[]);
                }

            },
            function(data,callback){
                //console.log(data);
                //async.each(data, function(id, eachcallback) {
                    db.collection('camp_stats').find({camp_id:{$in:data},processed:{$gte:1}}).sort({Date:-1}).toArray(function(err,res){
                        if(err){
                            //eachcallback(err);
                            callback(err);
                        }else{
                            //console.log(res);
                           //tdata= tdata.concat(res);
                           // console.log('ssssssssssss'+tdata);
                            //eachcallback(null);
                            callback(null,res);

                        }

                    });
                //},function(err){
                //    callback(null,tdata);
                //});
            },
            function(data,callback){
                var final_campaigns=[];

                //console.log(data.length);
                if(data.length > 0) {
                    async.each(data,function(id,callback){
                        var gmail_count = 0;
                        var yahoo_count = 0;
                        var total_inbox = 0;
                        var total_spam = 0;
                        var total_volume = 0;
                        var processed = 0;
                        var gpromotion,ypromotion;
                        var gstarred,ystarred;
                        var gsocial,ysocial;
                        var gupdates,yupdates;
                        var ginbox,yinbox;;
                        var gtrash,ytrash;
                        var gimportant,yimportant;
                        var gspam,yspam;
                        var gother,yother;
                        if (id.hasOwnProperty('gmail')) {
                            if (id.gmail.hasOwnProperty('promotion')) {
                                gpromotion = id.gmail.promotion;
                            }
                            else {
                                gpromotion = 0;
                            }
                            if (id.gmail.hasOwnProperty('starred')) {
                                gstarred = id.gmail.starred;
                            }
                            else {
                                gstarred = 0;
                            }
                            if (id.gmail.hasOwnProperty('social')) {
                                gsocial = id.gmail.social;
                            }
                            else {
                                gsocial = 0;
                            }
                            if (id.gmail.hasOwnProperty('updates')) {
                                gupdates = id.gmail.updates;
                            }
                            else {
                                gupdates = 0;
                            }
                            if (id.gmail.hasOwnProperty('inbox')) {
                                ginbox = id.gmail.inbox;
                            }
                            else {
                                ginbox = 0;
                            }
                            if (id.gmail.hasOwnProperty('trash')) {
                                gtrash = id.gmail.trash;
                            }
                            else {
                                gtrash = 0;
                            }
                            if (id.gmail.hasOwnProperty('important')) {
                                gimportant = id.gmail.important;
                            }
                            else {
                                gimportant = 0;
                            }
                            if (id.gmail.hasOwnProperty('spam')) {
                                gspam = id.gmail.spam;
                            }
                            else {
                                gspam = 0;
                            }
                            if (id.gmail.hasOwnProperty('other')) {
                                gother = id.gmail.other;
                            }
                            else {
                                gother = 0;
                            }
                            /*console.log(gspam+" "+gimportant+" "+gupdates+" "+gstarred+" "+gsocial+" "+gpromotion+" "+gtrash+" "+gother+" "+ginbox)*/
                        }
                        else {
                            ginbox = gspam = gimportant = gupdates = gstarred = gsocial = gpromotion = gtrash = gother = 0;
                        }
                        if (id.hasOwnProperty('yahoo')) {
                            if (id.yahoo.hasOwnProperty('promotion')) {
                                ypromotion = id.yahoo.promotion;

                            }
                            else {
                                ypromotion = 0;
                            }
                            if (id.yahoo.hasOwnProperty('starred')) {
                                ystarred = id.yahoo.starred;
                            }
                            else {
                                ystarred = 0;
                            }
                            if (id.yahoo.hasOwnProperty('social')) {
                                ysocial = id.yahoo.gsocial;
                            }
                            else {
                                ysocial = 0;
                            }
                            if (id.yahoo.hasOwnProperty('updates')) {
                                yupdates = id.yahoo.updates;
                            }
                            else {
                                yupdates = 0;
                            }
                            if (id.yahoo.hasOwnProperty('inbox')) {
                                yinbox = id.yahoo.inbox;
                            }
                            else {
                                yinbox = 0;
                            }
                            if (id.yahoo.hasOwnProperty('trash')) {
                                ytrash = id.yahoo.trash;
                            }
                            else {
                                ytrash = 0;
                            }
                            if (id.yahoo.hasOwnProperty('important')) {
                                yimportant = id.yahoo.important;
                            }
                            else {
                                yimportant = 0;
                            }
                            if (id.yahoo.hasOwnProperty('spam')) {
                                yspam = id.yahoo.spam;
                            }
                            else {
                                yspam = 0;
                            }
                            if (id.yahoo.hasOwnProperty('other')) {
                                yother = id.yahoo.other;
                            }
                            else {
                                yother = 0;
                            }
                        }
                        else {
                            yinbox = yspam = yimportant = yupdates = ystarred = ysocial = ypromotion = ytrash = yother = 0;
                        }
                        processed = id.processed;
                        gmail_count = (gupdates + gtrash + gimportant + ginbox + gspam + gsocial + gstarred + gpromotion + gother);
                        total_inbox = (gupdates + gtrash + gimportant + ginbox + gsocial + gstarred + gpromotion + gother + yupdates + ytrash + yimportant + yinbox + ysocial + ystarred + ypromotion + yother);
                        yahoo_count = (yupdates + ytrash + yimportant + yinbox + yspam + ysocial + ystarred + ypromotion + yother);
                        total_spam = (yspam + gspam);
                        total_volume = id.count;
                        //console.log(' jhnjkn'+gmail_count+" "+total_inbox+" "+yahoo_count+" "+total_spam+" "+processed+" "+total_volume);
                        db.collection("camp_master").findOne({_id: id.camp_id}, function (err, response) {
                            if (err) {
                                callback(null);
                            } else {
                                //console.log(response);

                                //console.log({sender: response.sender,subject:response.subject,gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed});
                                final_campaigns.push({sender: response.sender,subject:response.subject,gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed});
                                //if(final_campaigns.length === data.length){
                                //    //console.log(final_campaigns);
                                //    res.send(final_campaigns)
                                //}
                                callback(null);
                            }
                        })
                    },function(err){
                        callback(err,final_campaigns);
                    });


                }

                else{
                    callback(null,final_campaigns);
                }
            },
            function(data,callback){
                var result=[];
                async.eachSeries(data,function(id,callback){
                    db.collection("sender_master").findOne({_id: id.sender}, function (err, response) {
                        if(err){
                            callback(err);
                        }else{
                            id.sender=response.sender;
                            result.push(id);
                            callback(null);
                        }
                    });
                },function(err){
                    callback(err,result);
                });
            }
        ],function(err,data){
            //var final_campaigns=[];
            //
            ////console.log(data.length);
            //if(data.length > 0) {
            //    async.each(data,function(id,callback){
            //        var gmail_count = 0;
            //        var yahoo_count = 0;
            //        var total_inbox = 0;
            //        var total_spam = 0;
            //        var total_volume = 0;
            //        var processed = 0;
            //        var gpromotion,ypromotion;
            //        var gstarred,ystarred;
            //        var gsocial,ysocial;
            //        var gupdates,yupdates;
            //        var ginbox,yinbox;;
            //        var gtrash,ytrash;
            //        var gimportant,yimportant;
            //        var gspam,yspam;
            //        var gother,yother;
            //        if (id.hasOwnProperty('gmail')) {
            //            if (id.gmail.hasOwnProperty('promotion')) {
            //                gpromotion = id.gmail.promotion;
            //            }
            //            else {
            //                gpromotion = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('starred')) {
            //                gstarred = id.gmail.starred;
            //            }
            //            else {
            //                gstarred = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('social')) {
            //                gsocial = id.gmail.social;
            //            }
            //            else {
            //                gsocial = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('updates')) {
            //                gupdates = id.gmail.updates;
            //            }
            //            else {
            //                gupdates = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('inbox')) {
            //                ginbox = id.gmail.inbox;
            //            }
            //            else {
            //                ginbox = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('trash')) {
            //                gtrash = id.gmail.trash;
            //            }
            //            else {
            //                gtrash = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('important')) {
            //                gimportant = id.gmail.important;
            //            }
            //            else {
            //                gimportant = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('spam')) {
            //                gspam = id.gmail.spam;
            //            }
            //            else {
            //                gspam = 0;
            //            }
            //            if (id.gmail.hasOwnProperty('other')) {
            //                gother = id.gmail.other;
            //            }
            //            else {
            //                gother = 0;
            //            }
            //            /*console.log(gspam+" "+gimportant+" "+gupdates+" "+gstarred+" "+gsocial+" "+gpromotion+" "+gtrash+" "+gother+" "+ginbox)*/
            //        }
            //        else {
            //            ginbox = gspam = gimportant = gupdates = gstarred = gsocial = gpromotion = gtrash = gother = 0;
            //        }
            //        if (id.hasOwnProperty('yahoo')) {
            //            if (id.yahoo.hasOwnProperty('promotion')) {
            //                ypromotion = id.yahoo.promotion;
            //
            //            }
            //            else {
            //                ypromotion = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('starred')) {
            //                ystarred = id.yahoo.starred;
            //            }
            //            else {
            //                ystarred = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('social')) {
            //                ysocial = id.yahoo.gsocial;
            //            }
            //            else {
            //                ysocial = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('updates')) {
            //                yupdates = id.yahoo.updates;
            //            }
            //            else {
            //                yupdates = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('inbox')) {
            //                yinbox = id.yahoo.inbox;
            //            }
            //            else {
            //                yinbox = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('trash')) {
            //                ytrash = id.yahoo.trash;
            //            }
            //            else {
            //                ytrash = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('important')) {
            //                yimportant = id.yahoo.important;
            //            }
            //            else {
            //                yimportant = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('spam')) {
            //                yspam = id.yahoo.spam;
            //            }
            //            else {
            //                yspam = 0;
            //            }
            //            if (id.yahoo.hasOwnProperty('other')) {
            //                yother = id.yahoo.other;
            //            }
            //            else {
            //                yother = 0;
            //            }
            //        }
            //        else {
            //            yinbox = yspam = yimportant = yupdates = ystarred = ysocial = ypromotion = ytrash = yother = 0;
            //        }
            //        processed = id.processed;
            //        gmail_count = (gupdates + gtrash + gimportant + ginbox + gspam + gsocial + gstarred + gpromotion + gother);
            //        total_inbox = (gupdates + gtrash + gimportant + ginbox + gsocial + gstarred + gpromotion + gother + yupdates + ytrash + yimportant + yinbox + ysocial + ystarred + ypromotion + yother);
            //        yahoo_count = (yupdates + ytrash + yimportant + yinbox + yspam + ysocial + ystarred + ypromotion + yother);
            //        total_spam = (yspam + gspam);
            //        total_volume = id.count;
            //        //console.log(' jhnjkn'+gmail_count+" "+total_inbox+" "+yahoo_count+" "+total_spam+" "+processed+" "+total_volume);
            //        db.collection("camp_master").findOne({_id: id.camp_id}, function (err, response) {
            //            if (err) {
            //                callback(null);
            //            } else {
            //                //console.log(response);
            //
            //                //console.log({sender: response.sender,subject:response.subject,gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed});
            //                final_campaigns.push({sender: response.sender,subject:response.subject,gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed});
            //                //if(final_campaigns.length === data.length){
            //                //    //console.log(final_campaigns);
            //                //    res.send(final_campaigns)
            //                //}
            //                callback(null);
            //            }
            //        })
            //    },function(err){
            //        res.send(final_campaigns);
            //    });
            //
            //
            //}
            //
            //else{
            //    res.send([]);
            //}
//console.log(results);
            res.send(data);
        });

    });

    app.get('/alldata',function(req,res){
        var dat = getTime().dateStamp;
        db.collection('camp_stats').find({Date:dat,processed:{$gte:1}}).sort({Date: -1}).toArray(function(err,data){
            if(err){
                console.log(err);
            }else{
                //console.log(data);
                var final_campaigns=[];
                var farr=[];
                var i = 0;
            async.waterfall([
                function(callback){
                    if(data.length > 0) {
                        /*for (var i = 0; i < data.length; i++) {*/
                        async.each(data,function(id,callback){
                            var gmail_count = 0;
                            var yahoo_count = 0;
                            var total_inbox = 0;
                            var total_spam = 0;
                            var total_volume = 0;
                            var processed = 0;
                            var gpromotion,ypromotion;
                            var gstarred,ystarred;
                            var gsocial,ysocial;
                            var gupdates,yupdates;
                            var ginbox,yinbox;;
                            var gtrash,ytrash;
                            var gimportant,yimportant;
                            var gspam,yspam;
                            var gother,yother;
                            if (id.hasOwnProperty('gmail')) {
                                if (id.gmail.hasOwnProperty('promotion')) {
                                    gpromotion = id.gmail.promotion;
                                }
                                else {
                                    gpromotion = 0;
                                }
                                if (id.gmail.hasOwnProperty('starred')) {
                                    gstarred = id.gmail.starred;
                                }
                                else {
                                    gstarred = 0;
                                }
                                if (id.gmail.hasOwnProperty('social')) {
                                    gsocial = id.gmail.social;
                                }
                                else {
                                    gsocial = 0;
                                }
                                if (id.gmail.hasOwnProperty('updates')) {
                                    gupdates = id.gmail.updates;
                                }
                                else {
                                    gupdates = 0;
                                }
                                if (id.gmail.hasOwnProperty('inbox')) {
                                    ginbox = id.gmail.inbox;
                                }
                                else {
                                    ginbox = 0;
                                }
                                if (id.gmail.hasOwnProperty('trash')) {
                                    gtrash = id.gmail.trash;
                                }
                                else {
                                    gtrash = 0;
                                }
                                if (id.gmail.hasOwnProperty('important')) {
                                    gimportant = id.gmail.important;
                                }
                                else {
                                    gimportant = 0;
                                }
                                if (id.gmail.hasOwnProperty('spam')) {
                                    gspam = id.gmail.spam;
                                }
                                else {
                                    gspam = 0;
                                }
                                if (id.gmail.hasOwnProperty('other')) {
                                    gother = id.gmail.other;
                                }
                                else {
                                    gother = 0;
                                }
                                /*console.log(gspam+" "+gimportant+" "+gupdates+" "+gstarred+" "+gsocial+" "+gpromotion+" "+gtrash+" "+gother+" "+ginbox)*/
                            }
                            else {
                                ginbox = gspam = gimportant = gupdates = gstarred = gsocial = gpromotion = gtrash = gother = 0;
                            }
                            if (id.hasOwnProperty('yahoo')) {
                                if (id.yahoo.hasOwnProperty('promotion')) {
                                    ypromotion = id.yahoo.promotion;

                                }
                                else {
                                    ypromotion = 0;
                                }
                                if (id.yahoo.hasOwnProperty('starred')) {
                                    ystarred = id.yahoo.starred;
                                }
                                else {
                                    ystarred = 0;
                                }
                                if (id.yahoo.hasOwnProperty('social')) {
                                    ysocial = id.yahoo.gsocial;
                                }
                                else {
                                    ysocial = 0;
                                }
                                if (id.yahoo.hasOwnProperty('updates')) {
                                    yupdates = id.yahoo.updates;
                                }
                                else {
                                    yupdates = 0;
                                }
                                if (id.yahoo.hasOwnProperty('inbox')) {
                                    yinbox = id.yahoo.inbox;
                                }
                                else {
                                    yinbox = 0;
                                }
                                if (id.yahoo.hasOwnProperty('trash')) {
                                    ytrash = id.yahoo.trash;
                                }
                                else {
                                    ytrash = 0;
                                }
                                if (id.yahoo.hasOwnProperty('important')) {
                                    yimportant = id.yahoo.important;
                                }
                                else {
                                    yimportant = 0;
                                }
                                if (id.yahoo.hasOwnProperty('spam')) {
                                    yspam = id.yahoo.spam;
                                }
                                else {
                                    yspam = 0;
                                }
                                if (id.yahoo.hasOwnProperty('other')) {
                                    yother = id.yahoo.other;
                                }
                                else {
                                    yother = 0;
                                }
                            }
                            else {
                                yinbox = yspam = yimportant = yupdates = ystarred = ysocial = ypromotion = ytrash = yother = 0;
                            }
                            processed = id.processed;
                            gmail_count = (gupdates + gtrash + gimportant + ginbox + gspam + gsocial + gstarred + gpromotion + gother);
                            total_inbox = (gupdates + gtrash + gimportant + ginbox + gsocial + gstarred + gpromotion + gother + yupdates + ytrash + yimportant + yinbox + ysocial + ystarred + ypromotion + yother);
                            yahoo_count = (yupdates + ytrash + yimportant + yinbox + yspam + ysocial + ystarred + ypromotion + yother);
                            total_spam = (yspam + gspam);
                            total_volume = id.count;
                            //console.log(' jhnjkn'+gmail_count+" "+total_inbox+" "+yahoo_count+" "+total_spam+" "+processed+" "+total_volume);
                            db.collection("camp_master").findOne({_id: id.camp_id}, function (err, response) {
                                if (err) {
                                    callback(null);
                                } else {
                                    //console.log(response);

                                    //console.log({sender: response.sender,subject:response.subject,gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed});
                                    final_campaigns.push({sender: response.sender,subject:response.subject,gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed});
                                    //if(final_campaigns.length === data.length){
                                    //    //console.log(final_campaigns);
                                    //    res.send(final_campaigns)
                                    //}
                                    callback(null);
                                }
                            })
                        },function(err){
                            callback(err,final_campaigns);
                        });
                        /*   if (data[i].hasOwnProperty('gmail')) {
                         if (data[i].gmail.hasOwnProperty('promotion')) {
                         gpromotion = data[i].gmail.promotion;
                         }
                         else {
                         gpromotion = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('starred')) {
                         gstarred = data[i].gmail.starred;
                         }
                         else {
                         gstarred = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('social')) {
                         gsocial = data[i].gmail.social;
                         }
                         else {
                         gsocial = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('updates')) {
                         gupdates = data[i].gmail.updates;
                         }
                         else {
                         gupdates = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('inbox')) {
                         ginbox = data[i].gmail.inbox;
                         }
                         else {
                         ginbox = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('trash')) {
                         gtrash = data[i].gmail.trash;
                         }
                         else {
                         gtrash = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('important')) {
                         gimportant = data[i].gmail.important;
                         }
                         else {
                         gimportant = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('spam')) {
                         gspam = data[i].gmail.spam;
                         }
                         else {
                         gspam = 0;
                         }
                         if (data[i].gmail.hasOwnProperty('other')) {
                         gother = data[i].gmail.other;
                         }
                         else {
                         gother = 0;
                         }
                         /!*console.log(gspam+" "+gimportant+" "+gupdates+" "+gstarred+" "+gsocial+" "+gpromotion+" "+gtrash+" "+gother+" "+ginbox)*!/
                         }
                         else {
                         ginbox = gspam = gimportant = gupdates = gstarred = gsocial = gpromotion = gtrash = gother = 0;
                         }
                         if (data[i].hasOwnProperty('yahoo')) {
                         if (data[i].yahoo.hasOwnProperty('promotion')) {
                         ypromotion = data[i].yahoo.promotion;

                         }
                         else {
                         ypromotion = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('starred')) {
                         ystarred = data[i].yahoo.starred;
                         }
                         else {
                         ystarred = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('social')) {
                         ysocial = data[i].yahoo.gsocial;
                         }
                         else {
                         ysocial = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('updates')) {
                         yupdates = data[i].yahoo.updates;
                         }
                         else {
                         yupdates = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('inbox')) {
                         yinbox = data[i].yahoo.inbox;
                         }
                         else {
                         yinbox = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('trash')) {
                         ytrash = data[i].yahoo.trash;
                         }
                         else {
                         ytrash = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('important')) {
                         yimportant = data[i].yahoo.important;
                         }
                         else {
                         yimportant = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('spam')) {
                         yspam = data[i].yahoo.spam;
                         }
                         else {
                         yspam = 0;
                         }
                         if (data[i].yahoo.hasOwnProperty('other')) {
                         yother = data[i].yahoo.other;
                         }
                         else {
                         yother = 0;
                         }
                         }
                         else {
                         yinbox = yspam = yimportant = yupdates = ystarred = ysocial = ypromotion = ytrash = yother = 0;
                         }
                         processed = data[i].processed;


                         /!*console.log(yspam+" "+yimportant+" "+yupdates+" "+ystarred+" "+ysocial+" "+ypromotion+" "+ytrash+" "+yother+" "+yinbox);
                         *!/
                         gmail_count = (gupdates + gtrash + gimportant + ginbox + gspam + gsocial + gstarred + gpromotion + gother);
                         total_inbox = (gupdates + gtrash + gimportant + ginbox + gsocial + gstarred + gpromotion + gother + yupdates + ytrash + yimportant + yinbox + ysocial + ystarred + ypromotion + yother);
                         yahoo_count = (yupdates + ytrash + yimportant + yinbox + yspam + ysocial + ystarred + ypromotion + yother);
                         total_spam = (yspam + gspam);
                         total_volume = data[i].count;
                         farr.push({gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed,camp_id:data[i].camp_id});*/
                        //console.log(gmail_count+" "+total_inbox+" "+yahoo_count+" "+total_spam+" "+processed+" "+total_volume);
                        //console.log(farr);
                        //db.collection("camp_master").findOne({_id: data[i].camp_id}, function (err, response) {
                        //        if (err) {
                        //            throw err;
                        //        } else {
                        //            //console.log(response);
                        //
                        //             final_campaigns.push({sender: response.sender,subject:response.subject,gcount: gmail_count,ycount: yahoo_count,Tinbox: total_inbox,Tspam: total_spam,Tvolume: total_volume,processed: processed});
                        //            if(final_campaigns.length === data.length){
                        //                //console.log(final_campaigns);
                        //                res.send(final_campaigns)
                        //            }
                        //        }
                        //    });
                        //console.log("iiiiii",i,"dataaaa",data.length)


                        /*    }*/
                        //async.forEach(data,function(id,callback){});
                        //for(var j=0;j < farr.length;j++){
                        //    console.log('wwwwwwwwwwwwwwwwwwwwwwww:'+JSON.stringify(farr[j]));
                        /*   var j=0;
                         get_gcouts(j);
                         function get_gcouts(j) {
                         //console.log('weeeeeeeeeeeee',j);
                         db.collection("camp_master").findOne({_id: farr[j].camp_id}, function (err, response) {
                         if (err) {
                         throw err;
                         } else {
                         //console.log(j + "------------------->" + farr[j]);

                         final_campaigns.push({
                         sender: response.sender,
                         subject: response.subject,
                         gcount: farr[j].gcount,
                         ycount: farr[j].ycount,
                         Tinbox: farr[j].Tinbox,
                         Tspam: farr[j].Tspam,
                         Tvolume: farr[j].Tvolume,
                         processed: farr[j].processed
                         });
                         if (j < farr.length-1) {
                         j=j+1;
                         get_gcouts(j)
                         }else{
                         res.send(final_campaigns);
                         }
                         }
                         });
                         }*/
                    }

                    else{
                        callback(null,final_campaigns);
                    }
                },
                function(data,callback){
                    var result=[];
                    async.eachSeries(data,function(id,callback){
                        db.collection("sender_master").findOne({_id: id.sender}, function (err, response) {
                            if(err){
                                callback(err);
                            }else{
                                id.sender=response.sender;
                                result.push(id);
                                callback(null);
                            }
                        });
                    },function(err){
                        callback(err,result);
                    });
                }
            ],function(err,results){
                res.send(results);
            });

            }
        });
    });

    function getTime() {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        if (month < 10) {
            month = '0' + month;
        }
        var day = date.getDate();
        if (day < 10) {
            day = '0' + day;
        }
        var hour = date.getHours();
        if (hour < 10) {
            hour = '0' + hour;
        }
        var minute = date.getMinutes();
        if (minute < 10) {
            minute = '0' + minute;
        }
        var dt = {
            minuteStamp: Date.parse(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':00'),
            dateStamp: Date.parse(year + '-' + month + '-' + day + ' ' + '00' + ':' + '00' + ':00')
        };
        return dt;
    }

}