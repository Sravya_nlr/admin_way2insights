/**
 * Created by sys050 on 5/2/18.
 */
var express = require('express');
var router = express.Router();
var mongo=require('./MongoConnection');
var config=require("../config/labsConfig");
// var config=require("../config/liveConfig"); //uncomment for liveconfig
var db=null;
function getTime() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var day = date.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var hour = date.getHours();
    if (hour < 10) {
        hour = '0' + hour;
    }
    var minute = date.getMinutes();
    if (minute < 10) {
        minute = '0' + minute;
    }
    var dt = {
        minuteStamp: Date.parse(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':00'),
        dateStamp: Date.parse(year + '-' + month + '-' + day + ' ' + '00' + ':' + '00' + ':00')
    };
    return dt;
}
var request = require('request');

function queue_count(q,cb){
    var count_url = "http://test1:test1@95.211.177.3:15672/api/queues/%2f/" + q;
    request({
        url : count_url
    }, function(error, response, body) {
        //console.log("Called RabbitMQ API");
        if (error) {
            console.error("Unable to fetch Queued Msgs Count" + error);
            return;
        }
        else
        {
            var message = JSON.parse(body);

            if (message.hasOwnProperty("messages_ready")) {
                // this DOES NOT COUNT UnAck msgs
                var msg_ready = JSON.stringify(message.messages_ready);
                //console.log("message.messages_ready=" + msg_ready);
            }
            if (message.hasOwnProperty("messages")) {
                // _messages_ total messages i.e including unAck
                var msg = JSON.stringify(message.messages);
               // console.log("message.messages=" + msg);
                cb(null,msg)
            }
        }
    });
}

mongo.createConnection(config.url, function (err,db1) {
    if(err)
    {
        throw err;
    }else
    {
        console.log("connected to mongo..");
        db = db1;
    }

});
router.post('/getDashboardData',function(req,res){
 //  console.log("getDashboardData getDashboardData",req.body);
    var todayDate=req.body.date;
    // var todayDate=1506969000000;
    var finalObj={};
    processedCount();
    unProcessedOld();
    unProcessedNew();
    db.collection("ext2_data").count({db_date:todayDate},{"hint":{ "db_date" : 1,"toEmail" : 1}},function(err,count){
        if(err){
            throw err;
        }
        else{
            //console.log("count jsfuisf",count);
            finalObj["total"]=count;
            var keys=Object.keys(finalObj);
            if(keys.length === 4){
                res.send(finalObj);
            }
        }
    })
    function processedCount(){
        db.collection("ext2_data").count({db_date:todayDate,status:1},{"hint":{ "db_date" : 1,"toEmail" : 1}},function(err,count){
            if(err){
                throw err;
            }
            else{
                //console.log("count jsfuisf",count);
                finalObj["processed"]=count;
                var keys=Object.keys(finalObj);
                if(keys.length === 4){
                    res.send(finalObj);
                }
            }
        })
    }
    function unProcessedOld(){
        var q="OldQ";
        queue_count(q,function(err,resp){
            finalObj["old"]=resp;
            var keys=Object.keys(finalObj);
            if(keys.length === 4){
                res.send(finalObj);
            }
        });
    }
    function unProcessedNew(){
        var q="realQ";
        queue_count(q,function(err,resp){
            finalObj["Real"]=resp;
            var keys=Object.keys(finalObj);
            if(keys.length === 4){
                res.send(finalObj);
            }
        });
        // db.collection("ext2_data").count({db_date:todayDate,created_dt:todayDate, $or: [{status:26}]},{"hint":{ "db_date" : 1,"toEmail" : 1}},function(err,count){
        //     if(err){
        //         throw err;
        //     }
        //     else{
        //         //console.log("count jsfuisf",count);
        //         finalObj["Real"]=count;
        //         // console.log("final object", finalObj)
        //         var keys=Object.keys(finalObj);
        //         if(keys.length === 4){
        //             res.send(finalObj);
        //         }
        //
        //     }
        // })
    }
});
router.get('/active_stats',function(req,res){
    db.collection('active_stats').find({Date:getTime().dateStamp}).sort({Time:1}).toArray(function(err,resp){
        if(err){
            throw err;
        }
        else{
            //console.log(res)
            var timeArr=[];
            resp.forEach(function(doc){
                //var time=doc.Time.toTimeString
                //console.log(time);
                var t=doc.Time;
                var time=new Date(t);
                var onTime=time.toTimeString().substring(0, (time.toTimeString()).length-18)
                //console.log(onTime);
                timeArr.push({country:onTime,visits:doc.count})

            });
            if(timeArr.length === resp.length){
                res.send(timeArr);
            }
        }
    })
});
module.exports = router;
