var MongoClient = require('mongodb').MongoClient;
var db=null;
var mongo={
    createConnection:function(url,cb){
        if(!db)
        {
            MongoClient.connect(url,function(err,db)
            {
                if(err)
                    {
                        cb(err,null)
                    }
                else
                {
                    cb(null,db)

                }

            })
        }
        else{
            cb(null,db)
        }
      
        },
        closeConnection:function(){
            myDB.close();
        }

}
module.exports=mongo;
