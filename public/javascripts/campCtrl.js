app.controller('campCtrl',function($scope,$http,$cookieStore,$window){




    $scope.loader=0;
    $scope.limit=3;
    $scope.page=1;
    $scope.arr=[];
    $http({
        method: 'GET',
        url: '/alldata',
    }).then(function (response) {
        $scope.loader=1;
        //console.log('ssssssssssssssssss');
        //console.log(response.data);
        $scope.campdata=response.data;
        setTimeout(function(){

           $('#myTable').DataTable({
               "aaSorting": [],
               columnDefs: [
                   { orderable: false, targets:[0,1] }
               ]

           } );



        },100);



        var noofpages=Math.ceil($scope.campdata.length/$scope.limit);
       for(var j=1;j<=noofpages;j++){
           $scope.arr.push(j);
       }


    });

    $scope.search=function(sendersearch){
        $scope.loader=0;
        $scope.campdata=[];
        $('#myTable').DataTable().destroy();
        var obj = {sender: sendersearch};
        $http({
            method: 'GET',
            url: '/campdata',
            params: obj
        }).then(function (response) {
            $scope.loader=1;

            $scope.campdata=response.data;


            setTimeout(function(){
                //$scope.dataTable.clear();
                $('#myTable').DataTable({
                    "aaSorting": [],
                    columnDefs: [
                        { orderable: false, targets:[0,1] }
                    ]

                } );



        },1000);

        });
    }

    $scope.refreshbtn=function(){
        $scope.loader=0;
        $scope.campdata=[];
        $('#myTable').DataTable().destroy();
        $scope.arr=[];
        $http({
            method: 'GET',
            url: '/alldata',
        }).then(function (response) {
            $scope.loader=1;
            //console.log('ssssssssssssssssss');
            //console.log(response.data);
            $scope.campdata=response.data;
            setTimeout(function(){

                $('#myTable').DataTable({
                    "aaSorting": [],
                    columnDefs: [
                        { orderable: false, targets:[0,1] }
                    ]

                } );



            },100);

        });
    }
});