/**
 * Created by sys050 on 5/2/18.
 */

app.controller('mainCtrl', function ($scope, $http, $cookieStore, $cookies, $window,$interval) {
    // alert("in home controller")
    $scope.activeMenu = 'dash';
    $scope.m_active = function (page) {
        $scope.activeMenu = page
    };
    // $scope.active=""
    function loadData() {
        var cook = $cookies.get('encEmail');
        $scope.data = function (todayDate) {
            //alert(todayDate);
            $http({
                method: 'POST',
                url: '/dashboard/getDashboardData',
                data: {date: todayDate}
            }).then(function (response) {
                //alert(JSON.stringify(response));
                $scope.total = response.data.total;
                $scope.processed = response.data.processed;
                $scope.old = response.data.old;
                $scope.real = response.data.Real;

            });
        };
        if (cook) {
            $http({
                method: 'GET',
                url: '/cookie'
            }).then(function (response) {
                if(response.data.user){
                    $window.location = 'login2.html';
                    $scope.$apply(function () {
                        $scope.notUser="You are not our user";
                    });
                }
                else{
                    $scope.image = response.data.imageUrl;
                    $scope.name = response.data.profileName;
                }
            });
            var todayDate = getTime().dateStamp;
            $scope.data(todayDate);
        }
        else {
            $window.location = 'login2.html'
        }
    }

    loadData()
    // $interval(loadData, 1000);
    $scope.refresh = function () {
        loadData()
    };

    $scope.logout = function () {
        //alert("in logout")
        $http({
            method: 'GET',
            url: '/logout'
        }).then(function (response) {
            $window.location = 'login2.html'
        });
    };

});
app.controller('dashCtrl', function ($scope, $http, $cookieStore, $cookies, $window,$interval) {

    var arr=[];

    function barChart(){
        $http({
            method: 'GET',
            url: '/dashboard/active_stats'
        }).then(function (response) {
            // console.log(response.data)
            arr=response.data;
            var chart = AmCharts.makeChart( "chartdiv", {
                "type": "serial",
                "theme": "light",
                "dataProvider": arr,
                "valueAxes": [ {
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0
                } ],
                "gridAboveGraphs": false,
                "startDuration": 1,
                "graphs": [ {
                    "balloonText": "[[category]]: <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "visits"
                } ],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "country",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20
                }
            } );
        });
    }
    barChart();
    // $interval(barChart, 600000);
});