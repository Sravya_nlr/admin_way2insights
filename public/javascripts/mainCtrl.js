var app = angular.module('app',['ngRoute', 'ngCookies']);
app.config(function($routeProvider) {
    $routeProvider.when('/dashboard', {
            templateUrl: 'dashboard.html'
        })
        .when('/', {
            templateUrl: 'dashboard.html'
        })
        .when('/domain', {
            templateUrl: 'domains.html'
        })
        .when('/campaigns', {
            templateUrl: 'campaign.html',
            controller: 'campCtrl'
        })
        .when('/users', {
            templateUrl: 'userDetails.html'
        })

});
function getTime() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var day = date.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var hour = date.getHours();
    if (hour < 10) {
        hour = '0' + hour;
    }
    var minute = date.getMinutes();
    if (minute < 10) {
        minute = '0' + minute;
    }
    var dt = {
        minuteStamp: Date.parse(year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':00'),
        dateStamp: Date.parse(year + '-' + month + '-' + day + ' ' + '00' + ':' + '00' + ':00')
    };
    return dt;
}
app.controller('AuthCtrl', function($scope, $http,$window,$location,$cookieStore){
    // $scope.sravya="sravya";
    var localclientId="949169127216-hhl6rrs95hijgg20mbfi595i1vibdm0r.apps.googleusercontent.com";
    var liveClient="605060389986-m2k05rk5kl74nr2n6a8dofuhenrsd3d1.apps.googleusercontent.com";
    function attachSignin(element) {
        // console.log("sravyaaaaaaaaaaaaaaa")
        auth2.attachClickHandler(element, {},
            function (googleUser) {
                var email = googleUser.getBasicProfile().getEmail();
                var imageUrl = googleUser.getBasicProfile().getImageUrl();
                var profileName=googleUser.getBasicProfile().getName()

                // var domain = email.split("@")[1].split('.')[0];
                 //console.log("************************",email);
                // console.log("************************",googleUser.getBasicProfile());

                    $http({
                        method: 'POST',
                        url: '/email',
                        data: {email:email,imageUrl:imageUrl,profileName:profileName}
                    }).then(function (response) {
                        // $location.path('/chartjs');
                        if(response.data.user){
                            //alert(JSON.stringify(response.data))
                            $scope.$apply(function () {
                                $scope.notUser="You are not our user";
                            });
                        }
                        else{
                          //  alert("in index html");
                            $window.location='index.html';
                        }
                    });



            }, function (error) {
                // alert(JSON.stringify(error, undefined, 2));
                console.log(error);
            });
    }
    gapi.load('auth2', function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.

        auth2 = gapi.auth2.init({
            client_id: liveClient,
            apiKey: 'AIzaSyBxZMx4nOMw6ccgroEK5IluKQ34eua4MIA',
            cookiepolicy: 'single_host_origin',
            discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
            prompt: 'select_account'
        });
        attachSignin(document.getElementById('customBtn'));
    });

});
app.controller('homeCtrl',function($scope,$http,$cookieStore,$window){
    // alert("in home controller")
    $http({
        method: 'GET',
        url: '/cookie'
    }).then(function (response) {
        //alert(JSON.stringify(response));
        $scope.image=response.data.imageUrl;
        //alert(response.data.imageUrl);
        $scope.name=response.data.profileName;
        //$window.location='login2.html'
    });
    $scope.logout = function(){
        //alert("in logout")
        $http({
            method: 'GET',
            url: '/logout'
        }).then(function (response) {
            $window.location='login2.html'
        });
    };
    $scope.count=0;
    $scope.sorting = -1;
    $scope.sorting1 = 1;
    $scope.leng;
    $scope.table = true;
    $scope.fromDt = new Date();
    $scope.finalD = [];
    $scope.loadData=function() {
        $http.post('/dailydomains', {date: $scope.dateStamp,count: $scope.count,sort: $scope.sorting})
            .then(function (response) {
                /* console.log(response.data.a);*/
                $scope.finalD = JSON.parse(response.data.a);
                $scope.leng = $scope.finalD.length;
                $scope.table = true;
                $scope.table1 = false;

            });
    }
    $scope.showDate = function(){
        $scope.dateStamp = getTimeStamp();
        $scope.loadData();
    }
    $scope.showDate()

    $scope.change=function (data) {
        if(data==0&&$scope.count>0){
            $scope.count-=1;
        }
        else if(data==1){
            $scope.count+=1;
        }
        $scope.showDate();
    }
    function getTimeStamp() {
        var now = new Date($scope.fromDt);
        return Date.parse((now.getMonth() + 1) + '/' + (now.getDate()) + '/' + now.getFullYear());
    }
$scope.table1=false;
$scope.domainsearch=function(domain){
    $scope.domName=domain;
    $http.post('/DomainSearch', {domain: $scope.domName,date: getTimeStamp() })
        .then(function (response) {
            /* console.log(response.data.a);*/
            if(response.data.b !== "oops")
            {
            $scope.Daa = JSON.parse(response.data.b);
            $scope.table1 = true;
            $scope.table = false;
            }else{
                $scope.table1 = false;
                $scope.table = true;
            }
        });
}

$scope.keyCheck = function (domains,event) {
        $scope.key = event.keyCode;
        var domain_name = domains;
        if(($scope.key === 13)&&(domain_name !== undefined))
        {
            $scope.domainsearch(domain_name);
        }
    };


$scope.va = 0;
$scope.sort = function(value) {
    $scope.va += value;
    if (($scope.va % 2) == 0) {
        $scope.sorting = -1;
    }
    else {
        $scope.sorting = 1;
    }
    $scope.showDate();
};

    $scope.loadData1=function() {
        $http.post('/dailydomains1', {date: getTimeStamp(),count: $scope.count,sort: $scope.sorting1})
            .then(function (response) {
                /* console.log(response.data.a);*/
                $scope.finalD = JSON.parse(response.data.a);
                $scope.leng = $scope.finalD.length;
                $scope.table = true;
                $scope.table1 = false;

            });
    }

    $scope.va1 = 0;
    $scope.sort1 = function(value) {
        $scope.va1 += value;
        if (($scope.va1 % 2) == 0) {
            $scope.sorting1 = 1;
        }
        else {
            $scope.sorting1 = -1;
        }
        $scope.loadData1();
    };

});
