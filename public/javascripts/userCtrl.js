app.controller('userCtrl',function($scope,$http,$cookieStore,$window,$filter){
    $scope.fromDt = new Date();
    $scope.toDt=new Date();

    function getTimeStamp(dd) {
        var now = new Date(dd);
        return Date.parse((now.getMonth() + 1) + '/' + (now.getDate()) + '/' + now.getFullYear());
    }

    $scope.userData=function(){
        $('#userTable').DataTable().destroy();
        $http({
            url: '/users/userDetails',
            method: "GET",
            params: {fdate: $scope.fromDate,tdate:$scope.toDate}
        }).then(function (response) {
            var sdata = [];
            for(var len=0; len < response.data.length;len++){
                //var monlis = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

                var sdate=new Date(response.data[len].date.dateStamp);
                //var date = sdate.getDate();
                //var month = sdate.getMonth();
                //month = monlis[month];
                //var year = sdate.getFullYear();
                //var time=month +' '+date +' '+year;
                //var time=date+' '+month+' '+year;
                var time=   $filter('date')(sdate, 'yyyy/MM/dd')
                sdata.push({
                    name:response.data[len].name,
                    email:response.data[len].email,
                    gender:response.data[len].gender,
                    cdate:time
                });

            }

            $scope.userdata=sdata;
            setTimeout(function(){
                $('#userTable').DataTable({});
            },100);
        });
    }
    $scope.showDate = function(){
        //$scope.dateStamp1=123;

        $scope.fromDate = getTimeStamp($scope.fromDt);
        $scope.toDate=getTimeStamp($scope.toDt);
        //$scope.dateStamp = 12345567789;


        //alert("in show date");
        $scope.userData();
    }
    $scope.showDate();





});
